function Article() {
    return (
        <article>
            <div>
                <h3>Declarativo</h3>
                <p>Cras consectetur dui id fringilla facilisis. Nullam vitae augue sit amet sapien vulputate pretium sed sit amet nisi. Sed auctor ligula at cursus porttitor. Nulla facilisi. Nulla facilisi. In vehicula nibh gravida, commodo risus a, lacinia leo. Etiam ut congue neque.<br/>Duis id enim finibus, auctor magna sed, auctor neque. Fusce ut sodales neque. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Curabitur sed metus dolor.</p>
            </div>
            <div>
                <h3>Basado en componentes</h3>
                <p>Cras consectetur dui id fringilla facilisis. Nullam vitae augue sit amet sapien vulputate pretium sed sit amet nisi. Sed auctor ligula at cursus porttitor. Nulla facilisi. Nulla facilisi. In vehicula nibh gravida, commodo risus a, lacinia leo. Etiam ut congue neque.<br/>Duis id enim finibus, auctor magna sed, auctor neque. Fusce ut sodales neque. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Curabitur sed metus dolor.</p>
            </div>
            <div>
                <h3>Aprende una vez, escribelo donde sea</h3>
                <p>Cras consectetur dui id fringilla facilisis. Nullam vitae augue sit amet sapien vulputate pretium sed sit amet nisi. Sed auctor ligula at cursus porttitor. Nulla facilisi. Nulla facilisi. In vehicula nibh gravida, commodo risus a, lacinia leo. Etiam ut congue neque.<br/>Duis id enim finibus, auctor magna sed, auctor neque. Fusce ut sodales neque. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Curabitur sed metus dolor.</p>
            </div>
        </article>
    )
}

export default Article;