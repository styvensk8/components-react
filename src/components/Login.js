import { useState } from "react";

function Login() {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('')

    function Validation () {
        if(email === 'johan@correo.co' && password === '123') {
            console.log('Ingreso correctamente !')
        } else if (email === 'johan@correo.co' && password !== '123') {
            console.log( 'Contraseña incorrecta')
        } else {
            console.log('El correo regitrado no existe')
        }
    }

    return (
        <section>
            <form>
                <input type="text" value={email} onChange={(event) => setEmail(event.target.value)}></input>
                <input type="password" value={password} onChange={(event) => setPassword(event.target.value)}></input>
                <input type="button" value="Ingresar" onClick={Validation}></input>
            </form>
        </section>
    )
}

export default Login;