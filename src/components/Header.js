import Icon from "../assets/logo.svg"

function Header() {
    return (
        <header>
            <img src={Icon} alt="Logo de react" width='60px'/>
            <nav>
                <ul>
                    <li>Documentacion</li>
                    <li>Tutorial</li>
                    <li>Blog</li>
                    <li>Comunidad</li>
                </ul>
            </nav>
            <div>
                <form>
                    <input placeholder="Buscar" id="search" name="search"></input>
                </form>
                <ul>
                    <li>Version</li>
                    <li>Idioma</li>
                    <li>GitHub</li>
                </ul>
            </div>
        </header>
    )
}

export default Header;
