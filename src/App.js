import './App.css';
import Article from './components/Article';
import Header from './components/Header';
import Home from './components/Home'
import Count from './components/Count'

function App() {
  return (
    <div>
      <Header></Header>
      <Home></Home>
      <Article></Article>
      <Count></Count>
    </div>
  )
}

export default App;
